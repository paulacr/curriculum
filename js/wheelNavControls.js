$(document).ready(function(){
//WHEELNAV BEATRIXE
    
wheel = new wheelnav('wheelDiv');
wheel.slicePathFunction = slicePath().DonutSlice;
wheel.animatetime = 1000;
wheel.animateeffect = 'linear';
wheel.hoverPercent = 0.9;   
wheel.sliceSelectedTransformFunction= sliceTransform().MoveMiddleTransform;
wheel.wheelRadius = wheel.wheelRadius * 0.8;
wheel.colors = colorpalette.testy;
wheel.sliceSelectedAttr = {fill:'#5bc0de'};
wheel.createWheel([icon.magic,  icon.book, icon.wrench2, icon.apps]);
    
wheel.navItems[0].navigateFunction = function () {
    $('#mn').addClass('animated fadeIn');
    $('#mn').removeClass('hide');
    $('#mn').show(100);
    $('#knw').hide();
    $('#sklls').hide();
    $('#comp').hide();
    $('#pnl .main-heading').addClass('animated fadeIn');
    $('#pnl .main-heading').show(100);
    $('#mn').addClass('animated pulse');
    
};
    
wheel.navItems[1].navigateFunction = function () {
   $('#knw').addClass('animated fadeIn');
    $('#mn').hide();
    $('#knw').show();
    $('#knw').removeClass('hide');
    $('#sklls').hide();
    $('#comp').hide();
    $('#pnl .main-heading').hide();
};

    wheel.navItems[2].navigateFunction = function () {
    $('#sklls').addClass('animated fadeIn');
    $('#mn').hide();
    $('#knw').hide();
    $('#sklls').removeClass('hide');
    $('#sklls').show(100);
    $('#comp').hide();
    $('#pnl .main-heading').hide();
};
    wheel.navItems[3].navigateFunction = function () {
    $('#comp').addClass('animated fadeIn');
    $('#mn').hide();
    $('#knw').hide();
    $('#sklls').hide();
    $('#comp').show();
    $('#comp').removeClass('hide');
    $('#pnl .main-heading').hide();
};

    });