/**
 * Created by Paula on 2/6/2016.
 */

$(window).load(function () { // makes sure the whole site is loaded
	$('#status').fadeOut(); // will first fade out the loading animation
	$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
	$('body').delay(350).css({
		'overflow': 'visible'
	});


	$('.front-page img').addClass('animated fadeInDown');
	$('h1').addClass('animated fadeInLeft');

	$(window).stellar({
		horizontalScrolling: false
	});
});

$('body').scrollspy({
	target: '.bs-docs-sidebar',
	offset: 40
});

$('#sidebar').affix({
	offset: {
		top: $('#sidebar').offset().top
	}
});

$(function () {

	$('a[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

	//FROM TRAVIS :D

	//Cache reference to window and animation items
	var $animation_elements = $('.animation-element');
	var $window = $(window);

	$window.on('scroll resize', check_if_in_view);

	$window.trigger('scroll');

	function check_if_in_view() {
		var window_height = $window.height();
		var window_top_position = $window.scrollTop();
		var window_bottom_position = (window_top_position + window_height);

		$.each($animation_elements, function () {
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);

			//check to see if this current container is within viewport
			if ((element_bottom_position >= window_top_position) &&
				(element_top_position <= window_bottom_position)) {
				$element.addClass('in-view');
			} else {
				$element.removeClass('in-view');
			}
		});
	}

	//mobile swipe for nav
	$(".mobile").hide().css("backgroundColor", "rgba(255,255,255,0.9)").css("padding-top", "2em").css("margin-top", "-2.5em");
	$(".mobile-search").hide();
});

function swipedetect(el, callback) {

	var touchsurface = el,
		swipedir,
		startX,
		startY,
		distX,
		distY,
		threshold = 100, //required min distance traveled to be considered swipe
		restraint = 150, // maximum distance allowed at the same time in perpendicular direction
		allowedTime = 300, // maximum time allowed to travel that distance
		elapsedTime,
		startTime,
		handleswipe = callback || function (swipedir) {}

	touchsurface.addEventListener('touchstart', function (e) {
		var touchobj = e.changedTouches[0]
		swipedir = 'none'
		dist = 0
		startX = touchobj.pageX
		startY = touchobj.pageY
		startTime = new Date().getTime() // record time when finger first makes contact with surface
			//e.preventDefault()
	}, false)

	touchsurface.addEventListener('touchmove', function (e) {
		//e.preventDefault() // prevent scrolling when inside DIV
	}, false)

	touchsurface.addEventListener('touchend', function (e) {
		var touchobj = e.changedTouches[0]
		distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
		distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
		elapsedTime = new Date().getTime() - startTime // get time elapsed
		if (elapsedTime <= allowedTime) { // first condition for awipe met
			if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) { // 2nd condition for horizontal swipe met
				swipedir = (distX < 0) ? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
				e.preventDefault()
			} else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
				swipedir = (distY < 0) ? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
			}
		}
		handleswipe(swipedir)
			//e.preventDefault()
	}, false)
}

//USAGE:



window.addEventListener('load', function () {
	console.log('loaded')
	var el = document.querySelector('body')
	swipedetect(el, function (swipedir) {
		//swipedir contains either "none", "left", "right", "top", or "down"
		if (swipedir == 'left') {
			console.log('You just swiped left!')
			if ($(".mobile").css('display').toLowerCase() == 'block') {
				$(".mobile").hide("slide");
			} else {
				if ($(".mobile-search").css('display').toLowerCase() == 'none') {
					$(".mobile-search").show("slide", {
						direction: "right"
					}, 300);
				}
			}
		} else if (swipedir == 'right') {
			console.log('right')
			if ($(".mobile").css('display').toLowerCase() == 'none') {
				if ($(".mobile-search").css('display').toLowerCase() == 'none') {
					$(".mobile").show("slide");
				}

			}
			if ($(".mobile-search").css('display').toLowerCase() == 'block') {
				if ($(".mobile").css('display').toLowerCase() == 'none') {
					$(".mobile-search").hide("slide", {
						direction: "right"
					}, 300);
				}

			}
		}
	})
}, false)



// from dan, the burger menu and the search icon actions on click


$(document).ready(function () {


	$(".burger-menu").click(function() {
		if ($(".mobile").css('display').toLowerCase() == 'block') {
			$(".mobile").hide("slide");
		}
		if ($(".mobile").css('display').toLowerCase() == 'none') {
			if ($(".mobile-search").css('display').toLowerCase() == 'none') {
				$(".mobile").show("slide");
			}
		}

	});
	$(".mobile-search-icon").click(function() {


		if ($(".mobile-search").css('display').toLowerCase() == 'block') {
			$(".mobile-search").hide("slide", {
				direction: "right"
			}, 300);
		}
		if ($(".mobile-search").css('display').toLowerCase() == 'none') {
			if ($(".mobile").css('display').toLowerCase() == 'none') {
				$(".mobile-search").show("slide", {
					direction: "right"
				}, 300);
			}

		}
	});

//messing with particles.js

    /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
    particlesJS.load('particles-js', 'assets/particles.json', function() {
        console.log('callback - particles.js config loaded');
    });



//diploma - travis



var diplomaDiv = document.querySelector('#diplomaRow');
    
diplomaDiv.classList.add("test");


//Cache reference to window and animation items
	var $diploma_div = $('#diplomaRow');
	var $window = $(window);

	$window.on('scroll resize', check_if_diploma_in_view);

	$window.trigger('scroll');

    
    
	function check_if_diploma_in_view() {
		var window_height = $window.height();
		var window_top_position = $window.scrollTop();
		var window_bottom_position = (window_top_position + window_height);

		$.each($diploma_div, function () {
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);
            

			//check to see if this current container is within viewport
			if ((element_bottom_position >= window_top_position) &&
				(element_top_position <= window_bottom_position)) {
				$element.addClass('diplomaRock');
                $("style").append("#signature { stroke-dasharray: 1000; stroke-dashoffset: 1000; stroke-width: 1px; stroke: #000; fill: none; animation: dash 4.5s linear forwards; animation-delay: 2s;}#signature_2 { stroke-dasharray: 1000; stroke-dashoffset: 1000; stroke-width: 1px; stroke: #000; fill: none; animation: dash 4.5s linear forwards; animation-delay: 4s;} ");
			} else {
				$element.removeClass('diplomaRock');
                $("style").remove("#signature { stroke-dasharray: 1000; stroke-dashoffset: 1000; stroke-width: 1px; stroke: #000; fill: none; animation: dash 4.5s linear forwards; animation-delay: 2s;}");
               
			}
		});
	}
    
//document end
    });

